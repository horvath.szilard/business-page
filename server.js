const express       = require("express"),
      app           = express(),
      ejs           = require("ejs"),
      bodyParser    = require("body-parser"),
      nodemailer    = require("nodemailer"),
      port          = process.env.PORT || 3000;

let msgKapcsolat = "";

// EJS
app.set("view engine","ejs");

app.use('/', express.static(__dirname + '/public'));

// BodyParser
app.use(bodyParser.urlencoded({extended: true}));
/*
// 301 Átirányítás
app.get('*', function(req, res, next) {
    if (req.headers.host.slice(0, 3) != 'www') {
      res.redirect('https://www.' + req.headers.host + req.url, 301);
    } else {
      next();
    }
});
*/
app.get("/", (req, res) => {
    res.render("home", {msgKapcsolat: ""});
});

app.post("/", (req, res) => {
    // Email küldés
    let output = `
        <p>Új üzeneted érkezett a Bau Business oldalról:</p>
        <h3>Feladó adatai:</h3>
        <ul>
            <li>Név: ${req.body.nev}</li>
            <li>Email: ${req.body.email}</li>
            <li>Telefonszám: ${req.body.telefonszam}</li>
        </ul>
        <h3>Üzenet:</h3>
        <p>${req.body.uzenet}</p>
        `

    let transporter = nodemailer.createTransport({
        host: process.env.SMTP_HOST,
        port: process.env.SMTP_PORT,
        secureConnection: false,
        auth: {
            user: process.env.SMTP_USER,
            pass: process.env.SMTP_PASSWORD
        }
    });

    let mailOptions = {
        from: process.env.FROM_EMAIL,
        to: process.env.TO_EMAIL,
        subject: "Bau Business - Kapcsolat űrlap",
        html: output
    };

    transporter.sendMail(mailOptions, (err, info) => {
        if(err) {
            console.log(err);
            msgKapcsolat = "Az email elküldése közben valami hiba történt. Kérlek próbáld meg újra!";
            console.log(msgKapcsolat);
            res.render("home", {msgKapcsolat: msgKapcsolat});
        } else {
            console.log("Email elküldve: %s", info.messageId);
            msgKapcsolat = "Az email sikeresen elküldve.";
            console.log(req.body);
            res.render("home", {msgKapcsolat: msgKapcsolat});
    }});
});

app.get("/about", (req, res) => {
    res.render("about");
});

app.get("/privacy-policy", (req, res) => {
    res.render("privacy-policy");
});

app.use((req, res, next) => {
    res.status(404).render("404");
});
app.listen(port, () => {
    console.log(`A szerver a ${port} porton figyel...`);
});